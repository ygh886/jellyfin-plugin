﻿using Jellyfin.Plugin.JavMetadata.Site.AvMoo.Utility;
using MediaBrowser.Common.Net;
using MediaBrowser.Controller.Entities;
using MediaBrowser.Controller.Entities.Movies;
using MediaBrowser.Controller.Entities.TV;
using MediaBrowser.Controller.Providers;
using MediaBrowser.Model.Entities;
using MediaBrowser.Model.Providers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.AvMoo.Providers
{
    public class ImageProvider : IRemoteImageProvider
    {
        private readonly ILogger<ImageProvider> _logger;
        private readonly InfoHelper _infoHelper;

        public string Name => Plugin.AvMooProviderName;

        public string ProviderId => $"{Plugin.AvMooProviderName} Id"; 

        public ImageProvider(ILogger<ImageProvider> logger, InfoHelper infoHelper)
        {
            _logger = logger;
            _infoHelper = infoHelper;
        }

        public Task<HttpResponseMessage> GetImageResponse(
            string url,
            CancellationToken cancellationToken)
                => _infoHelper.GetResponse(url, cancellationToken);

        public async Task<IEnumerable<RemoteImageInfo>> GetImages(BaseItem item, CancellationToken cancellationToken)
        {
            var list = new List<RemoteImageInfo>();

            // 读取 AvMoo Id
            var id = item.GetProviderId(ProviderId);

            // 如果 AvMoo Id 为空，则返回空列表
            if (string.IsNullOrWhiteSpace(id))
            {
                _logger.LogWarning($"GetImages failed because that the sid is empty: {item.Name}");
                return list;
            }

            // 生成 详情页 url
            var url = $"https://{Plugin.Instance.Configuration.AvMooConfig.Domain}/{Plugin.Instance.Configuration.AvMooConfig.Language.ToString().ToLower()}/movie/{id}";

            // 下载 详情页 html 源码
            var html = await _infoHelper.GetHtml(url, cancellationToken);

            // 获取大封面 URL
            var urlFanart = await _infoHelper.GetFanartAsync(html, cancellationToken);

            if (!string.IsNullOrEmpty(urlFanart))
            {
                // 如果封面存在
                // 添加到图片列表

                // 小封面 poster
                list.Add(new RemoteImageInfo
                {
                    ProviderName = Name,
                    Url = urlFanart.Replace("pl", "ps"),
                    Type = ImageType.Primary
                });

                // 大封面 fanart/backdrop
                list.Add(new RemoteImageInfo
                {
                    ProviderName = Name,
                    Url = urlFanart,
                    Type = ImageType.Backdrop
                });

                // 列表为“缩略图”显示时，显示大封面
                list.Add(new RemoteImageInfo
                {
                    ProviderName = Name,
                    Url = urlFanart,
                    Type = ImageType.Thumb
                });
            }

            var screenshots = await _infoHelper.GetScreenshotsAsync(html, ProviderId, cancellationToken);

            list.AddRange(screenshots);

            return list;
        }

        public IEnumerable<ImageType> GetSupportedImages(BaseItem item)
        {
            yield return ImageType.Primary;
            yield return ImageType.Backdrop;
            yield return ImageType.Screenshot;
            yield return ImageType.Thumb;
        }

        public bool Supports(BaseItem item)
        {
            return item is Movie;
        }
    }
}
