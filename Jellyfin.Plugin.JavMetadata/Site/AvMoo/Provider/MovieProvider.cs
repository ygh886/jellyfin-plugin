﻿
using Jellyfin.Plugin.JavMetadata.Site.AvMoo.Utility;
using Jellyfin.Plugin.JavMetadata.Site.AvMoo.Dto;
using Jellyfin.Plugin.JavMetadata.Utility;
using MediaBrowser.Common.Net;
using MediaBrowser.Controller.Entities.Movies;
using MediaBrowser.Controller.Providers;
using MediaBrowser.Model.Entities;
using MediaBrowser.Model.Providers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.AvMoo.Provider
{
    public class MovieProvider : IHasOrder, IRemoteMetadataProvider<Movie, MovieInfo>
    {
        private readonly ILogger<MovieProvider> _logger;
        private readonly InfoHelper _infoHelper;

        public int Order => 3;

        public string Name => Plugin.AvMooProviderName;

        public string ProviderId => $"{Plugin.AvMooProviderName} Id";

        public MovieProvider(
            ILogger<MovieProvider> logger,
            InfoHelper infoHelper)
        {
            _logger = logger;
            _infoHelper = infoHelper;
        }

        public Task<HttpResponseMessage> GetImageResponse(
            string url,
            CancellationToken cancellationToken)
                => _infoHelper.GetResponse(url, cancellationToken);

        public async Task<MetadataResult<Movie>> GetMetadata(MovieInfo info, CancellationToken cancellationToken)
        {
            // 读取 AvMoo Id
            var id = info.GetProviderId(ProviderId);

            // 如果 AvMoo Id 为空，则根据标题重新获取，且默认使用结果的第一条数据
            if (string.IsNullOrWhiteSpace(id))
            {
                //var results = await GetIdsAsync(info.Name, cancellationToken);
                var results = (await _infoHelper.SearchMovieAsync(info.Name, cancellationToken).ConfigureAwait(false)).Select(item => item.Id);

                if (results.Count() > 0)
                {
                    id = results.FirstOrDefault();
                }
                else
                {
                    return new MetadataResult<Movie>();
                }
            }

            // 获取 元数据
            var movie = await _infoHelper.GetMetadata<Movie>(id, cancellationToken).ConfigureAwait(false);

            if (movie != null && movie.HasMetadata)
            {
                // 如果能获取到元数据，则把 AvMoo Id 设置为 当前 id
                info.SetProviderId(ProviderId, id);
            }

            return movie;
        }

        public async Task<IEnumerable<RemoteSearchResult>> GetSearchResults(MovieInfo searchInfo, CancellationToken cancellationToken)
        {
            var results = new List<RemoteSearchResult>();
            var ids = new List<string>();
            var searchResults = new List<SearchResult>();

            // 获取 AvMoo Id
            var AvMooId = searchInfo.GetProviderId(ProviderId);

            if (!string.IsNullOrEmpty(AvMooId))
            {
                // id 不为空，添加到 id 列表
                ids = new List<string>
                {
                    AvMooId
                };
            }
            else
            {
                // id 为空，则通过名称在线搜索并返回搜索结果的 id 列表
                //ids = (List<string>)await GetIdsAsync(searchInfo.Name, cancellationToken).ConfigureAwait(false);
                searchResults = (List<SearchResult>)await _infoHelper.SearchMovieAsync(searchInfo.Name, cancellationToken).ConfigureAwait(false);
                _logger.LogInformation($"search results count: {searchResults.Count}");
                ids = searchResults.Select(item => item.Id).ToList();
            }

            // 遍历 id 列表
            foreach (string id in ids)
            {
                // 获取 id 为 idItem 的影片详情
                var item = await _infoHelper.GetMovieDetailAsync(id, cancellationToken).ConfigureAwait(false);

                // 转换为 Jellyfin 查找结果(RemoteSearchResult)对象
                var searchResult = new RemoteSearchResult()
                {
                    Name = item.Title,
                    ImageUrl = item.Poster,
                    Overview = item.Intro,
                    SearchProviderName = Name
                };

                // 如果发行日期不为空，则设置年份
                if (item.ReleaseDate != null)
                {
                    searchResult.PremiereDate = item.ReleaseDate;
                    searchResult.ProductionYear = item.ReleaseDate?.Year;
                }

                // 设置 id
                searchResult.SetProviderId(ProviderId, id);

                // 添加到搜索结果列表
                results.Add(searchResult);
            }

            return results;
        }
    }
}
