﻿using MediaBrowser.Controller.Entities;
using MediaBrowser.Controller.Entities.Movies;
using MediaBrowser.Controller.Providers;
using MediaBrowser.Model.Entities;
using MediaBrowser.Model.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.AvMoo
{
    public class ExternalId : IExternalId
    {
        public string ProviderName => Plugin.AvMooProviderName;

        public string Key => "AvMoo Id";

        public ExternalIdMediaType? Type => null;

        public string UrlFormatString => $"https://{Plugin.Instance.Configuration.AvMooConfig.Domain}/{Plugin.Instance.Configuration.AvMooConfig.Language.ToString().ToLower()}/movie/{{0}}";

        public bool Supports(IHasProviderIds item)
        {
            return item is Movie || item is Video;
        }
    }
}
