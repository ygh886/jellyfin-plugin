﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Dto
{
    public class PersonModel
    {
        public string Name { get; set; }
        public string Alt { get; set; }
        public string Id { get; set; }
        public PersonAvatar Avatars { get; set; } = new PersonAvatar { };
    }
}
