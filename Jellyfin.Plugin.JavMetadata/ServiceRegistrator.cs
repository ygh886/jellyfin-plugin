﻿using Jellyfin.Plugin.JavMetadata.Utility;
using MediaBrowser.Common.Plugins;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata
{
    /// <summary>
    /// Register tmdb services.
    /// </summary>
    public class ServiceRegistrator : IPluginServiceRegistrator
    {
        /// <inheritdoc />
        public void RegisterServices(IServiceCollection serviceCollection)
        {
            // 注入
            serviceCollection.AddSingleton<HttpHelper>();
        }
    }
}
